# Installation des dépendances pour contruire l'application
```
sudo apt install npm
sudo npm i -g @angular/cli
```

# Commandes du starter Angular

## Ne pas oublier de changer le port d'écoute dans le .env

### Création d'une application Angular
```
./create-angular-app.sh
```

### Lancement du container en forcent le build
```
docker-compose up -d --build
```

### Lancement du container
```
docker-compose up -d
```

### Lancer une commande dans le container via un terminal (Linux)
```
docker exec -i [container_name] [command]
```

### Exemple
```
docker exec -i mon_container npm i
```

# Mise en preprod et prod
Pour mettre l'application Angular en preprod et en prod il faut utiliser le Dockerfile nommé `Dockerfile.production`. L'application doit être configurer pour être apte à tourner en mode production sur un serveur Nginx. Le port par défaut de Nginx et le port 80 et non le port de votre application en mode dev. Pensez donc à modifier le fichier `.env`.